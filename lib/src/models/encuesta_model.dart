// To parse this JSON data, do
//
//     final EncuestaModel = EncuestaModelFromJson(jsonString);

import 'dart:convert';

EncuestaModel encuestaModelFromJson(String str) =>
    EncuestaModel.fromJson(json.decode(str));

String encuestaModelToJson(EncuestaModel data) => json.encode(data.toJson());

class EncuestaModel {
  String aQuienVisita;
  String fueAtendido;
  String nombreEmpresa;
  String codEmpleado;
  String nombreEmpleado;
  String comentario;
  String inicio;
  String fin;
  double x;
  double y;

  EncuestaModel(
      {this.aQuienVisita = '',
      this.fueAtendido = 'NO',
      this.nombreEmpresa = '',
      this.codEmpleado = '',
      this.nombreEmpleado = '',
      this.comentario = '',
      this.inicio = '',
      this.fin = '',
      this.x = 0,
      this.y = 0});

  factory EncuestaModel.fromJson(Map<String, dynamic> json) =>
      new EncuestaModel(
          aQuienVisita: json["aQuienVisita"],
          fueAtendido: json["fueAtendido"],
          nombreEmpresa: json["nombreEmpresa"],
          codEmpleado: json["codEmpleado"],
          nombreEmpleado: json["nombreEmpleado"],
          comentario: json["comentario"],
          inicio: json["inicio"],
          fin: json["fin"],
          x: json["x"],
          y: json["y"]);

  Map<String, dynamic> toJson() => {
        "aQuienVisita": aQuienVisita,
        "fueAtendido": fueAtendido,
        "nombreEmpresa": nombreEmpresa,
        "codEmpleado": codEmpleado,
        "nombreEmpleado": nombreEmpleado,
        "comentario": comentario,
        "inicio": inicio,
        "fin": fin,
        "x": x,
        "y": y
      };
}
