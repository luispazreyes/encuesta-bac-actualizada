import 'dart:io';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:geolocator/geolocator.dart';

import 'package:flutter/material.dart';
import 'package:encuesta/src/bloc/provider.dart';

import 'package:encuesta/src/models/encuesta_model.dart';
import 'package:encuesta/src/utils/utils.dart' as utils;

class EncuestaPage extends StatefulWidget {
  @override
  _EncuestaPageState createState() => _EncuestaPageState();
}

class _EncuestaPageState extends State<EncuestaPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  EncuestasBloc encuestasBloc;
  EncuestaModel encuesta = new EncuestaModel();
  bool _guardando = false;
  // File foto;

  @override
  Widget build(BuildContext context) {
    encuestasBloc = Provider.encuestasBloc(context);

    final EncuestaModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) {
      encuesta = prodData;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Encuesta'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                // _mostrarFoto(),
                _crearNombreEmpresa(),
                _crearAQuienVisita(),
                _crearFueAtendido(),
                _crearNombreEmpleado(),
                _crearComentario(),
                _crearObtenerCoordenadas(),
                _crearBoton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearNombreEmpresa() {
    return TextFormField(
      initialValue: encuesta.nombreEmpresa,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre de la empresa'),
      onSaved: (value) => encuesta.nombreEmpresa = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre de la empresa';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearComentario() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: 8,
      maxLength: 1000,
      initialValue: encuesta.comentario,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Comentario'),
      onSaved: (value) => encuesta.comentario = value,
      // validator: (value) {
      //   if (value.length < 3) {
      //     return 'Ingrese el nombre de la empresa';
      //   } else {
      //     return null;
      //   }
      // },
    );
  }

  Widget _crearAQuienVisita() {
    return TextFormField(
      initialValue: encuesta.nombreEmpresa,
      textCapitalization: TextCapitalization.sentences,
      decoration:
          InputDecoration(labelText: 'Nombre de la persona a quien visita'),
      onSaved: (value) => encuesta.aQuienVisita = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre de la persona a quien visita';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearNombreEmpleado() {
    Map<String, String> empleados = {
      "40002089": "SANTOS HENRIQUEZ,ABRAHAN ISAU",
      "40004857": "ACOSTA MALDONADO,RICARDO ANTONIO",
      "40001448": "CENTENO ORDOÑEZ,MARCOS TULIO",
      "40001315": "AGUILAR ZELAYA,MARVIN JOEL",
      "40001163": "PEREZ FIGUEROA,ERIC JOEL",
      "40001502": "BELTRAN RODRIGUEZ,CARLOS ALCIDES",
      "40004901": "BARAHONA BENITEZ,MANUEL ALEJANDRO",
      "40005226": "JOSE ANGEL SERVELLON BANEGAS",
      "40007042": "FERNANDO LEONEL FLORES GALVEZ",
      "40006018": "BENAVIDES ORDOÑEZ,EDWARD ENRIQUE",
      "40001473": "RODEZNO OLIVA, ALEJANDRO ANTONIO"
    };

    return DropDownFormField(
      titleText: 'Empleado',
      hintText: 'Seleccione el nombre del empleado',
      value: encuesta.codEmpleado,
      onSaved: (value) {
        setState(() {
          encuesta.codEmpleado = value;
          encuesta.nombreEmpleado = empleados[value];
        });
      },
      onChanged: (value) {
        setState(() {
          encuesta.codEmpleado = value;
          encuesta.nombreEmpleado = empleados[value];
        });
      },
      dataSource: [
        {
          "display": "SANTOS HENRIQUEZ,ABRAHAN ISAU",
          "value": "40002089",
        },
        {
          "display": "ACOSTA MALDONADO,RICARDO ANTONIO",
          "value": "40004857",
        },
        {
          "display": "CENTENO ORDOÑEZ,MARCOS TULIO",
          "value": "40001448",
        },
        {
          "display": "AGUILAR ZELAYA,MARVIN JOEL",
          "value": "40001315",
        },
        {
          "display": "PEREZ FIGUEROA,ERIC JOEL",
          "value": "40001163",
        },
        {
          "display": "BELTRAN RODRIGUEZ,CARLOS ALCIDES",
          "value": "40001502",
        },
        {
          "display": "BARAHONA BENITEZ,MANUEL ALEJANDRO",
          "value": "40004901",
        },
        {
          "display": "JOSE ANGEL SERVELLON BANEGAS",
          "value": "40005226",
        },
        {
          "display": "FERNANDO LEONEL FLORES GALVEZ",
          "value": "40007042",
        },
        {
          "display": "BENAVIDES ORDOÑEZ,EDWARD ENRIQUE",
          "value": "40006018",
        },
        {
          "display": "RODEZNO OLIVA, ALEJANDRO ANTONIO",
          "value": "40001473",
        },
      ],
      textField: 'display',
      valueField: 'value',
    );
  }

  Widget _crearFueAtendido() {
    return SwitchListTile(
      value: encuesta.fueAtendido == 'NO' ? false : true,
      title: Text('¿Fue Atendido?'),
      activeColor: Colors.deepPurple,
      onChanged: (value) => setState(() {
        if (value) {
          encuesta.fueAtendido = 'SI';
        } else {
          encuesta.fueAtendido = 'NO';
        }
      }),
    );
  }

  Widget _crearBoton() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_validarCoordenadas()) ? null : _submit,
    );
  }

  Widget _crearObtenerCoordenadas() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Color.fromRGBO(200, 16, 47, 1.0),
      textColor: Colors.white,
      label: Text('Obtener Coordenadas'),
      icon: Icon(Icons.save),
      onPressed: _obtenerUbicacion,
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    encuestasBloc.agregarEncuesta(encuesta);

    // if (foto != null) {
    //   producto.fotoUrl = await productosBloc.subirFoto(foto);
    // }

    // if (producto.id == null) {
    // } else {
    //   productosBloc.editarEncuesta(producto);
    // }

    // setState(() {_guardando = false; });
    mostrarSnackbar('Registro guardado');

    Navigator.pop(context);
  }

  void mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _obtenerUbicacion() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    setState(() {
      encuesta.x = position.longitude;
      encuesta.y = position.latitude;
    });
  }

  bool _validarCoordenadas() {
    if (encuesta.x == 0.0 || encuesta.y == 0.0) {
      return true;
    } else {
      return false;
    }
  }
  // Widget _mostrarFoto() {
  //   if (producto.fotoUrl != null) {
  //     return FadeInImage(
  //       image: NetworkImage(producto.fotoUrl),
  //       placeholder: AssetImage('assets/jar-loading.gif'),
  //       height: 300.0,
  //       fit: BoxFit.contain,
  //     );
  //   } else {
  //     return Image(
  //       image: AssetImage(foto?.path ?? 'assets/no-image.png'),
  //       height: 300.0,
  //       fit: BoxFit.cover,
  //     );
  //   }
  // }

  // _seleccionarFoto() async {
  //   _procesarImagen(ImageSource.gallery);
  // }

  // _tomarFoto() async {
  //   _procesarImagen(ImageSource.camera);
  // }

  // _procesarImagen(ImageSource origen) async {
  //   foto = await ImagePicker.pickImage(source: origen);

  //   if (foto != null) {
  //     producto.fotoUrl = null;
  //   }

  //   setState(() {});
  // }
}
