import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:encuesta/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:encuesta/src/providers/encuestas_provider.dart';
import 'package:flutter/material.dart';
import 'package:encuesta/src/bloc/provider.dart';
import 'package:encuesta/src/models/encuesta_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _activo = false;
  EncuestasBloc encuestasBloc;
  EncuestaModel encuesta = new EncuestaModel();
  final _encuestasProvider = new EncuestaProvider();

  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    encuestasBloc = Provider.encuestasBloc(context);

    final EncuestaModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) {
      encuesta = prodData;
    }

    return Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Center(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child:
                _crearBotones() /*Image(
              image: AssetImage('assets/bacredomatic_logo.png'),
            )*/
            ),
      ),
      floatingActionButton: _crearBoton(context),
    );
  }

  _crearBoton(BuildContext context) {
    return FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(200, 16, 47, 1.0),
        onPressed: _activo ? _irEncuesta : null);
  }

  void _irEncuesta() {
    Navigator.pushNamed(context, 'encuesta');
  }

  Widget _crearBotones() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _crearNombreEmpleado(),
        _crearBotonIniciar(),
        _crearBotonParar(),
      ],
    );
  }

  Widget _crearBotonIniciar() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text('Iniciar Encuestas'),
      icon: Icon(Icons.save),
      onPressed: (!_activo)
          ? () {
              _prefs.horaInicio = DateTime.now().toString();
              setState(() {
                _activo = true;
              });
            }
          : null,
    );
  }

  Widget _crearBotonParar() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.green,
      textColor: Colors.white,
      label: Text('Detener'),
      icon: Icon(Icons.save),
      onPressed: _activo ? _grabarHoraFinal : null,
    );
  }

  void _grabarHoraFinal() {
    setState(() {
      _activo = false;
    });
    encuesta.inicio = _prefs.horaInicio;
    encuesta.codEmpleado = _prefs.codEmpleado;
    encuesta.nombreEmpleado = _prefs.nombreEmpleado;
    print('Probando:::::' + encuesta.inicio);
    _encuestasProvider.crearToken();
    encuesta.fin = DateTime.now().toString();
    encuestasBloc.agregarEncuesta(encuesta);
  }

  Widget _crearNombreEmpleado() {
    Map<String, String> empleados = {
      "40002089": "SANTOS HENRIQUEZ,ABRAHAN ISAU",
      "40004857": "ACOSTA MALDONADO,RICARDO ANTONIO",
      "40001448": "CENTENO ORDOÑEZ,MARCOS TULIO",
      "40001315": "AGUILAR ZELAYA,MARVIN JOEL",
      "40001163": "PEREZ FIGUEROA,ERIC JOEL",
      "40001502": "BELTRAN RODRIGUEZ,CARLOS ALCIDES",
      "40004901": "BARAHONA BENITEZ,MANUEL ALEJANDRO",
      "40005226": "JOSE ANGEL SERVELLON BANEGAS",
      "40007042": "FERNANDO LEONEL FLORES GALVEZ",
      "40006018": "BENAVIDES ORDOÑEZ,EDWARD ENRIQUE",
      "40001473": "RODEZNO OLIVA, ALEJANDRO ANTONIO"
    };

    return DropDownFormField(
      titleText: 'Empleado',
      hintText: 'Seleccione el nombre del empleado',
      value: encuesta.codEmpleado,
      onSaved: (value) {
        setState(() {
          encuesta.codEmpleado = value;
          encuesta.nombreEmpleado = empleados[value];
        });
      },
      onChanged: (value) {
        setState(() {
          encuesta.codEmpleado = value;
          encuesta.nombreEmpleado = empleados[value];
          _prefs.codEmpleado = value;
          _prefs.nombreEmpleado = empleados[value];
        });
      },
      dataSource: [
        {
          "display": "SANTOS HENRIQUEZ,ABRAHAN ISAU",
          "value": "40002089",
        },
        {
          "display": "ACOSTA MALDONADO,RICARDO ANTONIO",
          "value": "40004857",
        },
        {
          "display": "CENTENO ORDOÑEZ,MARCOS TULIO",
          "value": "40001448",
        },
        {
          "display": "AGUILAR ZELAYA,MARVIN JOEL",
          "value": "40001315",
        },
        {
          "display": "PEREZ FIGUEROA,ERIC JOEL",
          "value": "40001163",
        },
        {
          "display": "BELTRAN RODRIGUEZ,CARLOS ALCIDES",
          "value": "40001502",
        },
        {
          "display": "BARAHONA BENITEZ,MANUEL ALEJANDRO",
          "value": "40004901",
        },
        {
          "display": "JOSE ANGEL SERVELLON BANEGAS",
          "value": "40005226",
        },
        {
          "display": "FERNANDO LEONEL FLORES GALVEZ",
          "value": "40007042",
        },
        {
          "display": "BENAVIDES ORDOÑEZ,EDWARD ENRIQUE",
          "value": "40006018",
        },
        {
          "display": "RODEZNO OLIVA, ALEJANDRO ANTONIO",
          "value": "40001473",
        },
      ],
      textField: 'display',
      valueField: 'value',
    );
  }
}
