import 'package:shared_preferences/shared_preferences.dart';

/*
  Recordar instalar el paquete de:
    shared_preferences:

  Inicializar en el main
    final prefs = new PreferenciasUsuario();
    prefs.initPrefs();

*/

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET de la última página
  get token {
    return _prefs.getString('token') ?? '';
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  // GET y SET de la última página
  get horaInicio {
    return _prefs.getString('horaInicio') ?? '';
  }

  set horaInicio(String value) {
    _prefs.setString('horaInicio', value);
  }

  get codEmpleado {
    return _prefs.getString('codEmpleado') ?? '';
  }

  set codEmpleado(String value) {
    _prefs.setString('codEmpleado', value);
  }

  get nombreEmpleado {
    return _prefs.getString('nombreEmpleado') ?? '';
  }

  set nombreEmpleado(String value) {
    _prefs.setString('nombreEmpleado', value);
  }

  // GET y SET de la última página
  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'login';
  }

  set ultimaPagina(String value) {
    _prefs.setString('ultimaPagina', value);
  }
}
