import 'package:flutter/material.dart';

import 'package:encuesta/src/bloc/login_bloc.dart';
export 'package:encuesta/src/bloc/login_bloc.dart';

import 'package:encuesta/src/bloc/encuestas_bloc.dart';
export 'package:encuesta/src/bloc/encuestas_bloc.dart';

class Provider extends InheritedWidget {
  final loginBloc = new LoginBloc();
  final _encuestasBloc = new EncuestasBloc();

  static Provider _instancia;

  factory Provider({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = new Provider._internal(key: key, child: child);
    }

    return _instancia;
  }

  Provider._internal({Key key, Widget child}) : super(key: key, child: child);

  // Provider({ Key key, Widget child })
  //   : super(key: key, child: child );

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LoginBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(Provider) as Provider)
        .loginBloc;
  }

  static EncuestasBloc encuestasBloc(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>())
        ._encuestasBloc;
  }
}
